- [Español](./README.md#espa%C3%B1ol)
- [English](./README.md#english)

### Español
Herramienta para convertir los pasos con los que suelo empezar una maquina en una CTF y mas adelante poner opciones para maquina en RL(real life) la idea es que todo corra y vaya dando informacion para poder realizar varias tareas a la vez o simplemente dejarlo corriendo y hacer otras cosas, se usa la API/Libs de nmap, pero de momento gobuster/nikto etc se le llama desde GO, pero se cambiara para que sea natural del lenguage poco a poco, para poder usar el poder que tiene GO en herramientas de hackig que es MUCHA.

ejemplo:
```
./chikienumerator -t=10.10.10.146 -x=php,html,txt,sh
```
ayuda:
```
./chikienumerator -h
```

necesario:
- nmap
- nikto
- Gobuster
- BlackArch GNU/Linux ( recomendado pero cualquier GNU/Linux sobra )


A que es equivale este programa de momento:
1. nmap -sC -sV -oA target
2. nmap -script="http-* and not brute" target
3. nikto -h target
4. Gobuster

### English 

Tool for converting the steps with which I usually start a machine in a CTF and later put options for the machine in RL (real life) the idea is that everything runs and goes where information for power to realize various tasks at the same time or simply leave it running and do other things, you use the API/Libs of nmap, but at the moment gobuster/nikto etc is called from GO, but it will be changed so that it will be natural of the language little by little, in order to use the capabilities you have in GO tools for hacking, it's A LOT.

example:
	./chikienumerator -t=10.10.10.146 -x=php,html,txt,sh

help:
	./chikienumerator -h

needs:
- nmap
- nikto
- Gobuster
- BlackArch GNU/Linux (recommended, but any GNU/Linux works)

This is equivalent to these programs at the moment:
1. nmap -sC -sV -oA target
2. nmap --scrip9t="http-* and not brute" target
3. nikto -h target
4. Gobuster